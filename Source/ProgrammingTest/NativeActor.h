// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include <Components/StaticMeshComponent.h>
#include "InteractionInterface.h"
#include "NativeActor.generated.h"

UCLASS(NotBlueprintable)
class PROGRAMMINGTEST_API ANativeActor : public AActor, public IInteractionInterface
{
	GENERATED_BODY()
	
public:	
	ANativeActor();

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	UStaticMeshComponent* Mesh;

	virtual void Activate_Implementation(ACharacter* Character) override;

protected:
	void Report(ACharacter* Character);
};
