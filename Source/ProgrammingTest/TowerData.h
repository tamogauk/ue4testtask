// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "TowerData.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class PROGRAMMINGTEST_API UTowerData : public UDataAsset
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float GravityModifier;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float LowGravityModeDuration;
};
